var taskInput               = document.getElementById("taskList");//할 일 추가
var addButton               = document.getElementsByTagName("button")[0];//첫번째 button
var incompleteTask          = document.getElementById("incomplete");//미완료
var completedTasks          = document.getElementById("complete");//완료

//create Element & appending
var createNewTaskElement    = function(taskString){
    var listItem            = document.createElement("li");
    var checkBox            = document.createElement("input");
    var label               = document.createElement("label");
    var deleteButton        = document.createElement("button");
    label.innerText         = taskString;

    checkBox.type           = "checkbox";
    deleteButton.innerText  = "-";
    deleteButton.className  = "delete";

    listItem.appendChild(checkBox);
    listItem.appendChild(label);
    listItem.appendChild(deleteButton);

    return listItem;
}

//할 일 추가
var addTask         = function(){
    var listItem    = createNewTaskElement(taskInput.value);

    incompleteTask.appendChild(listItem);
    bindTaskEvents(listItem, taskCompleted);

    taskInput.value = "";
}

//할 일 삭제
var deleteTask      = function(){
    var listItem    = this.parentNode;
    var ul          = listItem.parentNode;

    ul.removeChild(listItem);
}

//완료
var taskCompleted   = function(){
    var listItem    = this.parentNode;

    completedTasks.appendChild(listItem);
    bindTaskEvents(listItem, taskIncomplete);
}

//미완료
var taskIncomplete  = function(){
    var listItem    = this.parentNode;

    incompleteTask.appendChild(listItem);
    bindTaskEvents(listItem, taskCompleted);
}

//할 일 추가 클릭 시 이벤트
addButton.addEventListener("click", addTask);

var bindTaskEvents          = function(taskListItem, checkBoxEventHandler){
    var checkBox            = taskListItem.querySelector("input[type=checkbox]");
    var deleteButton        = taskListItem.querySelector("button.delete");

    deleteButton.onclick    = deleteTask;
    checkBox.onchange       = checkBoxEventHandler;
}

//미완료 리스트
for (var i = 0; i < incompleteTask.children.length; i++){
    bindTaskEvents(incompleteTask.children[i], taskCompleted);
}

//완료 리스트
for (var i = 0; i < completedTasks.children.length; i++){
    bindTaskEvents(completedTasks.children[i], taskIncomplete);
}